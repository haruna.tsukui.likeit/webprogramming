<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー覧</title>
<!-- BootstrapのCSS読み込み -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- オリジナルCSS読み込み -->
    <link href="css/original/common.css" rel="stylesheet">
    <!-- Jqeryの読み込み -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
    <!-- BootstrapのJS読み込み -->
    <script src="js/bootstrap.min.js"></script>
    <!-- レイアウトカスタマイズ用個別CSS -->


</head>
<body>

	<!-- header -->
	<header>
		<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
			<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">${userInfo.name}</a>

			<ul class="navbar-nav px-3">
				<li class="nav-item text-nowrap"><a class="nav-link" href="LogoutServlet">ログアウト</a>
				</li>
			</ul>
		</nav>
	</header>
	<!-- /header -->

	<h1 class="text-center">ユーザ一覧</h1>
	<div class="col-xs-4" align="right">
		<a href="UserCreateServlet">新規登録</a>
	</div>

	<!-- 検索ボックス -->
	<div class="search-form-area">
		<div class="panel-body">
			<form method="post" action="UserListServlet" class="form-horizontal">
				<div class="form-group row">
					<label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="loginId"
							id="loginId">
					</div>
				</div>

				<div class="form-group row">
					<label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="userName"
							id="userName">
					</div>
				</div>

				<div class="form-group row">

					<label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>

					<div class="row col-sm-10">
						<div class="col-sm-5">
							<input type="date" name="birthDate" id="birthDate"
								class="form-control" />
						</div>

						<div class="col-sm-1 text-center">~</div>
						<div class="col-sm-5">
							<input type="date" name="date-end" id="date-end"
								class="form-control" />
						</div>
					</div>
				</div>
				<div class="text-right">
					<button type="submit" value="検索">検索</button>
				</div>
			</form>
		</div>
	</div>

	<!-- 検索結果一覧 -->
	<div class="table-responsive">
		<table class="table table-striped">
			<thead class="thead-dark">
				<tr>
					<th>ログインID</th>
					<th>ユーザ名</th>
					<th>生年月日</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			<c:forEach var="user" items="${userList}" >
				<tr>
					<td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
					<td>
					 <c:if test="${userInfo.loginId=='admin'}">
                       <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                       <a class="btn btn-danger"  href ="UserDeleteServlet?id=${user.id}">削除</a>
                       </c:if>

                      <c:if test="${!(userInfo.loginId=='admin')}">
                      <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>

                       <c:if test="${userInfo.loginId==user.loginId}">

                      	<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                      </c:if>

                      </c:if>

				</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>

</body>
</html>