<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ削除画面</title>
<!-- BootstrapのCSS読み込み -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- オリジナルCSS読み込み -->
  <link href="css/original/common.css" rel="stylesheet">
</head>
<body>
<!-- header -->
  <header>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">${userInfo.name}</a>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="LogoutServlet">ログアウト</a>
        </li>
      </ul>
    </nav>
  </header>
  <!-- /header -->

  <!-- body -->
  <div class="container">

  <p>ログインID：${user.loginId}</p>
  <p>を本当に削除してよろしいでしょうか。</p>
  <form method="post" action="UserDeleteServlet">
  <input type="hidden" name="id" value="${user.id}">
  <div class="col-xs-4">
        <a href="UserListServlet" class="btn btn-light">キャンセル</a>
        <p> <input type="submit" value="OK"></p>
      </div>
      </form>
</body>
</html>