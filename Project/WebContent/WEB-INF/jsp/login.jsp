<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/original/login.css" rel="stylesheet">
</head>

<body>
	<div class="login">
		<h1>ログイン画面</h1>

		<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

		<form method="post" action="LoginServlet">
			<p>ログインID 　　<input type="text" name="loginId"></p>
			<p>パスワード　　 <input type="text" name="password"></p>
			<p> <input type="submit" value="ログイン"></p>
		</form>
	</div>
</body>
</html>